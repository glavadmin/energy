unit LifeForm;

interface

uses
  System.Types, System.UITypes, System.Classes, System.Math,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Objects;
const
   MaxLifeWidth=384;
   MaxLifeHeight=512;

type

TLifeSpace=packed array [-MaxLifeWidth..MaxLifeWidth,-MaxLifeHeight..MaxLifeHeight] of TAlphaColorF;
  PLifeSpace=TLifeSpace;
  TLife=object
    constructor Create;
    destructor Destroy;
  private
    procedure Execute;
{  public
    Pause:boolean;
    Progress:boolean;   }
  end;


  TLifeForm1 = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  LifeForm1: TLifeForm1;
  MyBitmap: TBitmap;
 // LifeWidth:integer;
 // LifeHeight:integer;
  LifeSpace,OldLife:PLifeSpace;
  Life:TLife;
  MyBitmapCopy: TBitmap;

implementation
uses CalcUnit;
var
   calc1:calc;
{$R *.fmx}
constructor Tlife.Create;
var
 i,j:integer;
begin
// LifeWidth:=MaxLifeWidth;
// LifeHeight:=MaxLifeHeight;
// if not life.Progress then
// begin
  //New(LifeSpace);
 // New(OldLife);
 // Progress:=True;
  //Pause:=False;

  //for i:=-MaxLifeWidth to MaxLifeWidth do
  // for j:=-MaxLifeHeight to MaxLifeHeight do LifeSpace[i,j] := 0;
 { for i:=-MaxLifeWidth to MaxLifeWidth do
   for j:=-MaxLifeHeight to MaxLifeHeight do OldLife^[i,j] := 0;     }
 //end;

end;

procedure Tlife.Execute;
var
// cur,o:Cardinal;
 //t:pointer;
 i,j:integer;
 k,tmp:integer;
 x,y,z:extended;
 gx: array [1..5] of Extended;
 gy: array [1..5] of Extended;
 gz: array [1..5] of Extended;
 r,ff:integer;
begin
// repeat
  //for ff := 1 to 2 do
 // begin


 x:=0;
 y:=0;
 z:=0;
 r:=round(4)+1;
 for k:=1 to r do
 begin
  gx[k]:=random*random/15;
  gy[k]:=random*random/15;
  gz[k]:=random*random/15;
 end;
 for i:=-MaxLifeWidth  to MaxLifeWidth do
  for j:=-MaxLifeHeight to MaxLifeHeight do
  begin
   for k:=1 to r do
   begin
    x:=cos(gx[k]*(cos(i/50)+sin(j/50))*100+30*pi*gx[k]*sin(x));
    y:=sin(gy[k]*(cos(j/50)+sin(i/50))*100+30*pi*gy[k]*cos(y));
    z:=sin(gz[k]*(cos(i/50)+sin(j/50))*100+30*pi*gz[k]*cos(z));
   end;
  LifeSpace[i,j]:= TAlphaColorF.Create(x,y,z);
   // t:=LifeSpace;
  //LifeSpace:=OldLife;
 // OldLife:=t;
//end;
end;

end;

destructor Tlife.Destroy;
begin
// if life.Progress then
// begin
//  Progress:=False;
  //Dispose(LifeSpace);
  //Dispose(OldLife);
 //end;
end;



procedure TLifeForm1.FormCreate(Sender: TObject);
begin
  Life.Create;
 // Life.StandardAdd;
//  calc1:=Calc.Create;
  MyBitmap := TBitmap.Create(0,0);
  MyBitmap.Assign(Image1.Bitmap);
  if (LifeForm1.Height<LifeForm1.Width) then image1.RotationAngle:=90;
  Life.Execute;
  Timer1.Enabled:=true;
end;

procedure TLifeForm1.Image1Click(Sender: TObject);
begin
 Timer1.Enabled:=false;
 Life.Execute;
 Timer1.Enabled:=true;
end;

procedure TLifeForm1.Timer1Timer(Sender: TObject);
var
  X, Y: Integer;
  c:Cardinal;
  ModifiedData : TBitmapData;
begin
  MyBitmapCopy := TBitmap.Create(0, 0);
  MyBitmapCopy.Assign(MyBitmap);
  try
    // Changes the color of certain pixels.
    // This changes the transparency of the pixels that have the amount of Red.
    // lower then the curent value of the trackbar
    if (MyBitmapCopy.Map(TMapAccess.Write,ModifiedData)) then
    begin
      try
        MyBitmapCopy.Clear(4278190080);

        for X := 0 to Image1.Bitmap.Width - 1 do
          for Y := 0 to Image1.Bitmap.Height - 1 do
            begin
              ModifiedData.SetPixel(X, Y, LifeSpace[-MaxLifeWidth+X,-MaxLifeHeight+Y].ToAlphaColor);
            end;

      finally
        MyBitmapCopy.Unmap(ModifiedData);
      end;
    end;
    Image1.Bitmap := MyBitmapCopy;
    Image1.Repaint;
  finally
    MyBitmapCopy.Free;
  end;
  //Timer1.Enabled:=false;
end;

end.